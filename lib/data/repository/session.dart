import 'package:app/domain/model/usuario.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SessionApi {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<Usuario> auth(Usuario usuario) async {
    try {
      final User user = (await _auth.signInWithEmailAndPassword(
        email: usuario.email,
        password: usuario.senha,
      )).user;

      return Usuario(email: user.email);
    } catch (e) {
      throw e;
    }
  }

  Future<Usuario> signUp(Usuario usuario) async {
    try {
      final User user = (await _auth.createUserWithEmailAndPassword(
        email: usuario.email,
        password: usuario.senha,
      )).user;

      return Usuario(email: user.email);
    } catch (e) {
      throw e;
    }
  }
}
