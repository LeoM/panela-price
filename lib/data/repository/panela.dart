import 'package:app/domain/model/panela.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class PanelaApi {
  final CollectionReference query =
      FirebaseFirestore.instance.collection('panelas');

  Future<List<Panela>> getPanelas() async {
    try {
      final response = await query.get();

      return response.docs.map((panela) {
        return Panela.fromJson({
          ...panela.data(),
          ...{'id': panela.id}
        });
      }).toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> createPanela(String nome, String imagem, double preco, int vendas) async {
    try {
      final User user = FirebaseAuth.instance.currentUser;
      query
          .add({
            'nome': nome,
            'imagem': imagem,
            'preco': preco,
            'vendas': vendas,
            'autor': user.email
          })
          .then((value) => print("Panela criada com sucesso!"))
          .catchError((error) => print("Falha ao criar panela: $error"));
    } catch (e) {
      throw e;
    }
  }

  Future<void> deletePanela(String id) async {
    try {
      query
        .doc(id)
        .delete()
        .then((value) => print("Panela excluída"))
        .catchError((error) => print("Falha ao excluir panela: $error"));
    } catch (e) {
      throw e;
    }
  }
}
