class Usuario {
  Usuario({
    this.id,
    this.email,
    this.senha,
  });

  int id;
  String email;
  String senha;

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        id: json["id"] == null ? null : json["id"],
        email: json["email"] == null ? null : json["email"],
        senha: json["senha"] == null ? null : json["senha"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "email": email == null ? null : email,
        "senha": senha == null ? null : senha,
      };
}
