class Panela {
  Panela(
      {this.preco, this.imagem, this.vendas, this.id, this.nome, this.autor});

  double preco;
  String imagem;
  int vendas;
  String id;
  String nome;
  String autor;

  factory Panela.fromJson(Map<String, dynamic> json) {
    String defaultImage =
        "https://media-exp1.licdn.com/dms/image/C4E0BAQGkNqJroEC3nA/company-logo_200_200/0?e=2159024400&v=beta&t=I84UhuQL7NKkKIH-tfJ6r5nsY15g4FUNPnRSKFt3bt8";
    bool isNullOrEmpty(String value) {
      return value == null || value.isEmpty ? true : false;
    }

    return Panela(
        preco: json["preco"] == null ? null : json["preco"].toDouble(),
        imagem: isNullOrEmpty(json["imagem"]) ? defaultImage : json["imagem"],
        vendas: json["vendas"] == null ? null : json["vendas"],
        id: json["id"] == null ? null : json["id"],
        nome: isNullOrEmpty(json["nome"]) ? "Não cadastrado" : json["nome"],
        autor: isNullOrEmpty(json["autor"]) ? 'Não cadastrado' : json["autor"]);
  }

  Map<String, dynamic> toJson() => {
        "preco": preco == null ? null : preco,
        "imagem": imagem == null ? null : imagem,
        "vendas": vendas == null ? null : vendas,
        "id": id == null ? null : id,
        "nome": nome == null ? null : nome,
        "autor": autor == null ? null : autor
      };
}
