import 'package:app/app/page/createPanela.dart';
import 'package:app/app/page/home.dart';
import 'package:app/app/page/index.dart';
import 'package:app/app/page/login.dart';
import 'package:app/app/page/signUp.dart';
import 'package:app/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      // case settingsRoute:
      //   return SlideRightRoute(
      //       widget: ConfigPage(
      //     appBarColor: Color(0xff1D2745),
      //     gateway: true,
      //     aplicativo: 'mais kpi',
      //   ));
      case loginPage:
        return SlideRightRoute(widget: LoginPage());
      case signUpPage:
        return SlideRightRoute(widget: SignUpPage());
      case homePage:
        return SlideRightRoute(widget: HomePage());
      case createPanelaPage:
        return SlideRightRoute(widget: CreatePanelaPage());
      default:
        return SlideRightRoute(widget: IndexPage());
    }
  }
}

class SlideRightRoute extends PageRouteBuilder {
  final Widget widget;
  SlideRightRoute({this.widget})
      : super(
            pageBuilder: (BuildContext context, Animation<double> animation,
                Animation<double> secondaryAnimation) {
              return widget;
            },
            transitionsBuilder: (BuildContext context,
                    Animation<double> animation,
                    Animation<double> secondaryAnimation,
                    Widget child) =>
                FadeTransition(opacity: animation, child: child),
            transitionDuration: Duration(milliseconds: 500));
}
