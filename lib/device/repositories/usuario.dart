import 'package:app/device/repositories/database.dart';
import 'package:app/domain/model/usuario.dart';

class UsuarioTableHandler {
  final DatabaseHandler database = new DatabaseHandler();

  Future<Usuario> select() async {
    try {
      var db = await database.db;
      List<Map<String, dynamic>> list = await db.transaction((txn) async {
        return await txn.query('usuario');
      });

      return Usuario.fromJson(list[0]);
    } catch (e) {
      return null;
    }
  }

  Future<bool> create(Usuario usuario) async {
    try {
      var db = await database.db;
      await db.transaction((txn) async {
        return await txn.insert('usuario', usuario.toJson());
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> deleteAll() async {
    try {
      var db = await database.db;
      await db.transaction((txn) async {
        return await txn.delete('usuario');
      });
      return true;
    } catch (e) {
      return false;
    }
  }
}
