const String usuarioTable = '''
  CREATE TABLE IF NOT EXISTS usuario (
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    email TEXT,
    senha TEXT
  );
''';
