import 'package:app/constants.dart';
import 'package:app/data/repository/panela.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
part 'panela.g.dart';

class PanelaController = PanelaControllerBase with _$PanelaController;

abstract class PanelaControllerBase with Store {
  @observable
  bool loading = false;

  TextEditingController nome = new TextEditingController();
  TextEditingController imagem = new TextEditingController();
  TextEditingController preco = new TextEditingController(text: "0");
  TextEditingController vendas = new TextEditingController(text: "0");

  @action
  createPanela(BuildContext context) async {
    try {
      this.loading = true;
      final api = PanelaApi();
      print('teste ${vendas.text}');
      await api.createPanela(nome.text, imagem.text, double.parse(preco.text), int.parse(vendas.text));

      Navigator.pushNamedAndRemoveUntil(context, homePage, (route) => false);
    } catch (e) {
      print(e);
    } finally {
      this.loading = false;
    }
  }
}
