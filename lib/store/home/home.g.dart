// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$panelasAtom = Atom(name: '_HomeControllerBase.panelas');

  @override
  ObservableList<Panela> get panelas {
    _$panelasAtom.reportRead();
    return super.panelas;
  }

  @override
  set panelas(ObservableList<Panela> value) {
    _$panelasAtom.reportWrite(value, super.panelas, () {
      super.panelas = value;
    });
  }

  final _$usuarioAtom = Atom(name: '_HomeControllerBase.usuario');

  @override
  Usuario get usuario {
    _$usuarioAtom.reportRead();
    return super.usuario;
  }

  @override
  set usuario(Usuario value) {
    _$usuarioAtom.reportWrite(value, super.usuario, () {
      super.usuario = value;
    });
  }

  final _$nivelBateriaAtom = Atom(name: '_HomeControllerBase.nivelBateria');

  @override
  String get nivelBateria {
    _$nivelBateriaAtom.reportRead();
    return super.nivelBateria;
  }

  @override
  set nivelBateria(String value) {
    _$nivelBateriaAtom.reportWrite(value, super.nivelBateria, () {
      super.nivelBateria = value;
    });
  }

  final _$getUserAsyncAction = AsyncAction('_HomeControllerBase.getUser');

  @override
  Future getUser() {
    return _$getUserAsyncAction.run(() => super.getUser());
  }

  final _$getPanelasAsyncAction = AsyncAction('_HomeControllerBase.getPanelas');

  @override
  Future getPanelas() {
    return _$getPanelasAsyncAction.run(() => super.getPanelas());
  }

  final _$exitAsyncAction = AsyncAction('_HomeControllerBase.exit');

  @override
  Future exit(BuildContext context) {
    return _$exitAsyncAction.run(() => super.exit(context));
  }

  @override
  String toString() {
    return '''
panelas: ${panelas},
usuario: ${usuario},
nivelBateria: ${nivelBateria}
    ''';
  }
}
