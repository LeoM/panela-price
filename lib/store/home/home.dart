import 'package:app/constants.dart';
import 'package:app/data/repository/panela.dart';
import 'package:app/device/repositories/usuario.dart';
import 'package:app/domain/model/panela.dart';
import 'package:app/domain/model/usuario.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobx/mobx.dart';
part 'home.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  @observable
  ObservableList<Panela> panelas = new ObservableList.of([]);

  @observable
  Usuario usuario;

  @observable
  String nivelBateria;

  static const platform = const MethodChannel('samples.flutter.dev/battery');

  _getBatteryLevel() async {
    try {
      final int result = await platform.invokeMethod('getBatteryLevel');
      nivelBateria = '$result %';
      print('bateria $result');
    } on PlatformException catch (e) {}
  }

  @action
  getUser() async {
    _getBatteryLevel();
    final usuarioTableHandler = UsuarioTableHandler();
    this.usuario = await usuarioTableHandler.select();
  }

  @action
  getPanelas() async {
    try {
      final api = PanelaApi();

      this.panelas = ObservableList.of(await api.getPanelas());

      print(this.panelas);
    } catch (e) {
      print(e);
    }
  }

  @action
  exit(BuildContext context) async {
    final usuarioTableHandler = UsuarioTableHandler();
    await usuarioTableHandler.deleteAll();

    Navigator.pushNamedAndRemoveUntil(context, indexPage, (route) => false);
  }
}
