// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'session.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SessionController on _SessionControllerBase, Store {
  final _$loadingAtom = Atom(name: '_SessionControllerBase.loading');

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  final _$authAsyncAction = AsyncAction('_SessionControllerBase.auth');

  @override
  Future auth(BuildContext context) {
    return _$authAsyncAction.run(() => super.auth(context));
  }

  final _$signUpAsyncAction = AsyncAction('_SessionControllerBase.signUp');

  @override
  Future signUp(BuildContext context) {
    return _$signUpAsyncAction.run(() => super.signUp(context));
  }

  @override
  String toString() {
    return '''
loading: ${loading}
    ''';
  }
}
