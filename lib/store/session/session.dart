import 'package:app/constants.dart';
import 'package:app/data/repository/session.dart';
import 'package:app/device/repositories/usuario.dart';
import 'package:app/domain/model/usuario.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
part 'session.g.dart';

class SessionController = _SessionControllerBase with _$SessionController;

abstract class _SessionControllerBase with Store {
  @observable
  bool loading = false;

  TextEditingController email = new TextEditingController();
  TextEditingController senha = new TextEditingController();

  @action
  auth(BuildContext context) async {
    try {
      this.loading = true;
      final sessionApi = new SessionApi();
      final table = UsuarioTableHandler();

      final Usuario usuario = await sessionApi
          .auth(Usuario(email: this.email.text, senha: this.senha.text));

      if (await table.deleteAll()) {
        await table.create(usuario);
      }

      Navigator.pushNamedAndRemoveUntil(context, homePage, (route) => false);
    } catch (e) {
      print(e);
    } finally {
      this.loading = false;
    }
  }

  @action
  signUp(BuildContext context) async {
    try {
      this.loading = true;
      final sessionApi = new SessionApi();
      final table = UsuarioTableHandler();

      final Usuario usuario = await sessionApi
          .signUp(Usuario(email: this.email.text, senha: this.senha.text));

      if (await table.deleteAll()) {
        await table.create(usuario);
      }

      Navigator.pushNamedAndRemoveUntil(context, homePage, (route) => false);
    } catch (e) {
      print(e);
    } finally {
      this.loading = false;
    }
  }
}
