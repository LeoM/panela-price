import 'package:app/domain/model/panela.dart';
import 'package:flutter/material.dart';

class CardPanela extends StatelessWidget {
  final Panela panela;
  final Function() onPressRemove;

  CardPanela({this.panela, this.onPressRemove});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 250,
        child: Card(
          elevation: 8,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("${panela.nome}"),
                      IconButton(
                        padding: EdgeInsets.zero,
                        constraints: BoxConstraints(),
                        icon: Icon(Icons.close),
                        iconSize: 28,
                        onPressed: () => onPressRemove()
                      )
                    ],
                  ),
                ),
                Expanded(
                    child: Image.network(
                  panela.imagem,
                  fit: BoxFit.cover,
                )),
                Padding(padding: EdgeInsets.only(bottom: 8)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text("Preço: ${panela.preco}"),
                    Text("QTD Vendas: ${panela.vendas}"),
                    Text("Autor: ${panela.autor}"),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
