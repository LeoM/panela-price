import 'package:app/app/widget/card-panela.dart';
import 'package:app/constants.dart';
import 'package:app/data/repository/panela.dart';
import 'package:app/domain/model/panela.dart';
import 'package:app/store/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final homeController = HomeController();
  PanelaApi panelaApi = PanelaApi();

  @override
  void initState() {
    super.initState();
    homeController.getUser();
    homeController.getPanelas();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Panela Price"),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Center(child: Observer(builder: (_) {
                return Text(homeController?.usuario?.email ?? '');
              })),
            ),
            IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () {
                  homeController.exit(context);
                })
          ],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20.0),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Observer(builder: (_) {
                    return Text(
                      "Nível da bateria: ${homeController.nivelBateria ?? ''}",
                      style: TextStyle(color: Colors.white),
                    );
                  })
                ],
              ),
            ),
          )),
      body: Observer(
        builder: (_) {
          return RefreshIndicator(
            onRefresh: () async {
              await homeController.getPanelas();
              return null;
            },
            child: ListView.builder(
                itemCount: homeController.panelas.length,
                itemBuilder: (context, index) {
                  Panela panela = homeController.panelas[index];
                  return CardPanela(
                    panela: panela,
                    onPressRemove: () => onPressRemove(panela.id, index),
                  );
                }),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {Navigator.pushNamed(context, createPanelaPage)},
        tooltip: 'Add New',
        child: Icon(Icons.add),
        backgroundColor: Colors.cyan[900],
      ),
    );
  }

  void onPressRemove(String id, int index) async {
    await panelaApi.deletePanela(id);
    homeController.panelas.removeAt(index);
  }
}
