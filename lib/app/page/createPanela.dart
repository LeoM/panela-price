import 'package:app/store/panela/panela.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class CreatePanelaPage extends StatefulWidget {
  @override
  _CreatePanelaPageState createState() => _CreatePanelaPageState();
}

class _CreatePanelaPageState extends State<CreatePanelaPage> {
  final panelaController = PanelaController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cadastrar Panela"),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
        child: Column(
          children: [
            TextField(
              controller: panelaController.nome,
              decoration: InputDecoration(filled: true, labelText: 'Nome'),
            ),
            Padding(padding: EdgeInsets.all(5)),
            TextField(
              controller: panelaController.imagem,
              decoration: InputDecoration(filled: true, labelText: 'URL da imagem'),
            ),
            Padding(padding: EdgeInsets.all(5)),
            TextField(
              controller: panelaController.preco,
              obscureText: false,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r"\d+([\.]\d+)?"))],
              decoration: InputDecoration(filled: true, labelText: 'Preço'),
            ),
            Padding(padding: EdgeInsets.all(5)),
            TextField(
              controller: panelaController.vendas,
              obscureText: false,
              keyboardType: TextInputType.number,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: InputDecoration(filled: true, labelText: 'Qtd de vendas'),
            ),
            Padding(padding: EdgeInsets.all(5)),
            Container(
              height: 50,
              width: double.infinity,
              child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  onPressed: () {
                    if (!panelaController.loading) {
                      panelaController.createPanela(context);
                    }
                  },
                  child: Observer(
                    builder: (_) {
                      return panelaController.loading
                          ? SizedBox(
                              height: 20,
                              width: 20,
                              child: CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Colors.white),
                              ),
                            )
                          : Text("Cadastrar Panela");
                    },
                  )),
            )
          ],
        ),
      ),
    );
  }
}
