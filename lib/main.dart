import 'package:app/app/page/home.dart';
import 'package:app/app/page/index.dart';
import 'package:app/device/repositories/usuario.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:app/router.dart' as r;

var defaultPage;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  final usuario = await UsuarioTableHandler().select();

  if (usuario != null) {
    defaultPage = HomePage();
  } else {
    defaultPage = IndexPage();
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Panela Price',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: defaultPage,
        onGenerateRoute: r.Router.generateRoute);
  }
}
